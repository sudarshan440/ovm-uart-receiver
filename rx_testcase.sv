class rx_testcase extends ovm_test;
rx_sequence seq;
rx_sequencer sequencer;
rx_environment env;
//registering
`ovm_component_utils(rx_testcase)

//constructor
function new(string name = " ", ovm_component parent= null);
super.new(name,parent);

endfunction : new

function void build();
  super.build();
  env = rx_environment::type_id::create("env",this);
endfunction : build

task run();
$cast(sequencer,env.agent.sequencer);
sequencer.count = 0;
  seq = rx_sequence::type_id::create("seq"); 
seq.start(sequencer,null);
endtask 

endclass : rx_testcase