class rx_agent extends ovm_agent;
rx_monitor_tb  monitortb;
rx_monitor_dut  monitordut;
rx_driver driver;
rx_sequencer sequencer;

ovm_analysis_port #(rx_sequence_item) agenttb;
ovm_analysis_port #(rx_sequence_item) agentdut;

//registering
`ovm_component_utils(rx_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

function void build();
super.build();
agenttb = new("agenttb",this);
agentdut = new("agentdut",this);

sequencer = rx_sequencer::type_id::create("sequencer",this);
driver = rx_driver::type_id::create("driver",this);
monitortb = rx_monitor_tb::type_id::create("monitortb",this);
monitordut = rx_monitor_dut::type_id::create("monitordut",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(sequencer.seq_item_export);
monitortb.montb2sb.connect(agenttb);
monitordut.mondut2sb.connect(agentdut);

endfunction : connect


endclass : rx_agent