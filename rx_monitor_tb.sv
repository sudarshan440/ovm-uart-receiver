class rx_monitor_tb extends ovm_monitor;
rx_sequence_item seq_item;
virtual rx_intf intf;
Wrapper wrapper;
ovm_object dummy;

ovm_analysis_port #(rx_sequence_item) montb2sb;
//registering
`ovm_component_utils(rx_monitor_tb )

//coverage collector
//covergroup cg;
//???????????????
//endgroup : cg 

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
//cg = new;
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
montb2sb = new("montb2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

integer state = 0,count=0;
logic [7:0] temp_register;
bit data_register  = 0,data_received= 0;


virtual task run();
seq_item = rx_sequence_item::type_id::create("seq_item");

forever
begin

case (state)
  0: begin
      #80ns
       if (intf.rxd == 0)
         begin 
 
         state  = 1;
	     $display("rxd zer detected");
        end

  
     end
  1: 
     begin
	          if (count < 8)
               begin
					#80ns;

					$display("in rx monitor tb data is %d",intf.rxd  );  

					temp_register  = {intf.rxd,temp_register[7:1]};
				//  $display("tempdata is %d",tempdata);			
					count = count + 1;
					$display("counter is %d", count);
					end
			else
			begin 
					count = 0; 
					$display("temp_register is %d",temp_register);			
					state = 2; 
			end
	
	 end
	 
	 2:
	 
	 begin
	     $display("state is %d",state);
			seq_item.rx_data = temp_register   ;
		$display("seq_item.rx_data is %d",seq_item.rx_data);
		montb2sb.write(seq_item); 
		state = 0;

	 end
     
endcase 
end

endtask : run


/*
virtual task run();
forever
begin


if ( intf.rxd == 1'b0)
begin
 state = 1;
 data_received  = 1;
end

if (state == 1)
begin

   if (count < 8 )
    begin
     @(posedge intf.rx_clk)
     //temp_register = temp_register.push_front(intf.rxd);
    // #70ns  
      temp_register = {intf.rxd,temp_register[7:1]};
      count=count + 1;
    end
	else
	 begin
     // #70ns
	  count = 0;
	    if (intf.txd == 1)
          state = 2;	 
	 end
end

 

if (state == 2)
begin
 state = 0;
 seq_item.rx_data = temp_register[7:0];
 //cg.sample();
model(); 
montb2sb.write(seq_item);

end


end //forever loop
endtask : run
*/
function void model();
$display("dummy model ");
endfunction : model
endclass : rx_monitor_tb