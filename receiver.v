// Code your design here
module receiver (clk, reset,rxdata,rxd);
parameter idle = 1'b0;
parameter receiving = 1'b1;

input clk,reset;
input rxd;
output reg [7:0] rxdata;
reg shift;
reg  state,nextstate;
reg [3:0] bitcounter;
reg [3:0] samplecounter;
reg [12:0] counter;
reg [10:0] rxshiftreg;
reg clear_bitcounter,inc_bitcounter,clear_samplecounter,inc_samplecounter,done;




always @ (posedge clk)
begin
        
 if (reset) begin
    state <= idle;
    bitcounter <= 0;
   counter <= 0;
   samplecounter <= 0;
   rxshiftreg <= 0; 
  end

  else begin
       counter <= counter +1 ;
    if (counter >= 3 ) //1736
      begin 
        counter <= 0;
        state <= nextstate;
        rxshiftreg <=  {rxd,rxshiftreg[10:1]};
		$display("rxd is %d",rxd);
        if (clear_samplecounter) samplecounter <= 0;
      else if (inc_samplecounter) samplecounter <= samplecounter + 1;
     end
   end
end

always @ (state or rxd or samplecounter) 
begin

  shift = 0;
clear_samplecounter = 0;
inc_samplecounter = 0;
clear_bitcounter = 0;
clear_bitcounter = 0;
inc_bitcounter = 0;
rxdata = 0;
  done = 0;
case (state)
 idle : begin
            if (rxd)
              nextstate = idle;
           else  begin
               nextstate = receiving;
               clear_bitcounter = 1;
                clear_samplecounter = 1;
            end
         end
  receiving : begin

            
    if (samplecounter == 10) begin 
               
            clear_samplecounter = 1;
            rxdata = rxshiftreg[8:1];
                         done  = 1;
         //rxdata = 0;
		 $display("at dut rxdata is %d",rxdata);
           end
            else
                 inc_samplecounter = 1;
           end 
endcase

end

endmodule             


 





 