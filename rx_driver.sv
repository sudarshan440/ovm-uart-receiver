class rx_driver extends ovm_driver#(rx_sequence_item);
rx_sequence_item seq_item;
virtual rx_intf intf;
Wrapper wrapper;
ovm_object dummy;

//registering
`ovm_component_utils(rx_driver)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

//run phase
virtual task run ();
reset_dut();
forever 
begin


 //   #70ns
 
repeat(10)
begin
//#70ns

seq_item_port.get_next_item(seq_item);
#80ns

 //@(posedge intf.rx_clk)
intf.rxd = seq_item.rxd;
seq_item_port.item_done;
end // repeat loop 

//#70ns

 
end //forever loop
endtask : run



task reset_dut();
       intf.rx_rst = 1;
	   intf.rxd = 1;
       #50ns ;
       intf.rx_rst = 0;

     endtask
endclass : rx_driver 