module top;
  import rxPkg::*;
  import rxTestPkg::*;
rx_intf intf();

initial
begin
intf.rx_clk = 0;
end

always #10 intf.rx_clk = ~intf.rx_clk;

receiver dut (.clk(intf.rx_clk),
                 .reset(intf.rx_rst),
                // .receive(intf.receive),
                 .rxd(intf.rxd),
                 .rxdata(intf.rx_data)				 
);


initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("rx_testcase");
end


endmodule : top