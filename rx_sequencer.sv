class rx_sequencer extends ovm_sequencer#(rx_sequence_item);

//registering
`ovm_component_utils(rx_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : rx_sequencer