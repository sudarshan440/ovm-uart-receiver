class rx_sequence_item extends ovm_sequence_item;
// variable declaration 
rand bit rxd;
bit [7:0] rx_data;
// registering with factory
`ovm_object_utils(rx_sequence_item)

//constructor
function new(string name = " " );
super.new(name);
endfunction: new

//declaration of constraints

endclass : rx_sequence_item