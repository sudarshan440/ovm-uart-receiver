class rx_sequence extends ovm_sequence#(rx_sequence_item);
rx_sequence_item seq_item;

//registering 
`ovm_object_utils(rx_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();



seq_item = rx_sequence_item::type_id::create("seq_item");
wait_for_grant();
seq_item.randomize() with {rxd == 1'b0;};
send_request(seq_item);
wait_for_item_done();
  
repeat(8)
begin
seq_item = rx_sequence_item::type_id::create("seq_item");
wait_for_grant();
seq_item.randomize();
send_request(seq_item);
wait_for_item_done();
  
end

seq_item = rx_sequence_item::type_id::create("seq_item");
wait_for_grant();
seq_item.randomize() with {rxd == 1'b1;};
send_request(seq_item);
wait_for_item_done();
  
endtask : body
endclass : rx_sequence